const router = require('express').Router();
const { addCard, addMessage } = require("../controller/card.controller")

router.post("/addCard", addCard);
router.post("/addMessage", addMessage);


module.exports = router;