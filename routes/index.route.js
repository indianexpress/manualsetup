const router = require('express').Router();

router.use('/user',require("./user.route"))
router.use('/card',require("./card.route"))

module.exports = router;