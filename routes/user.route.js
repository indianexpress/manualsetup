const router = require('express').Router();
const protect = require("../middleware/user.protect");
const { register, updateDetails } = require("../controller/user.controller")

router.post("/register", register);
router.post("/updateUser", updateDetails);

module.exports = router;