const express = require('express');
const app = express();
const dotenv = require('dotenv')
dotenv.config()
const port = process.env.PORT || 3000;
const mongoose = require("mongoose");
const helemt = require('helmet');
var bodyParser = require('body-parser')
const logger = require('morgan');


app.use(helemt());
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(logger('dev'));
app.use(bodyParser.json())
// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });
app.get('/', (req, res) => {
    res.redirect('/checkServer')
});
app.use('/api', require('./routes/index.route'));
app.get('/', (req, res) => {
    res.send("hello world");
})

app.listen(port, () => {
    console.log(`server is listening on port : ${port}`)
})

mongoose.connect(process.env.DBURL).then(res => {
    console.log(`server is connected `)
}).catch(err => {
    console.log(err, "server is not connected")
})