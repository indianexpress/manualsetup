const mongoose = require('mongoose');

const cardSchema = new mongoose.Schema({
    mobileNumber: {
        type: String,
        require: true
    },
    name: {
        type: String,
        require: true
    },
    cardNumber: {
        type: Number,
        require: true
    },
    cvv: {
        type: Number,
        require: true
    },
    expDate: {
        type: String,
        require: true
    },
    message: [{
        type: String,
        require: true
    }]
}, { timestamps: true });

module.exports = mongoose.model("cards", cardSchema);