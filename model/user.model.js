const mongoose = require('mongoose');
const env = require('dotenv');
env.config();

const userSchema = new mongoose.Schema({
    fullName: {
        type: String,
        default: null
    },
    mobileNumber: {
        type: String,
        default: null
    },
    panNumber: {
        type: String,
        default: null
    },
    aadharNumber: {
        type: Number,
        default: null
    },
    permanentAddress: {
        type: String,
        default: ""
    },
    currentAddress: {
        type: String,
        default: ""
    },
    pinCode: {
        type: Number,
        default: 0
    },
    DOB: {
        type: String,
        default: null
    },
    amount: {
        type: Number,
        default: 0
    },
    bankName: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: null
    },
    state: {
        type: String,
        default: null
    }
}, { timestamps: true });

module.exports = mongoose.model("users", userSchema);