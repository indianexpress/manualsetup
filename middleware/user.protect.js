const userModal = require("../model/user.model");
const jwt = require("jsonwebtoken");
const env = require("dotenv");
env.config();

exports.protect = async (req, res, next) => {
    try {
        let token = ""
        if (req.headers.authorization && req.header.authorization.startsWith('Bearer')) {
            token = req.header.authorization.split(" ")[1]
        }
        if(!token){
            return res.json({
                code : 401,
                message : "provide token in header"
            })
        }
    const dataFromToken = jwt.verify(token,process.env.JWT_SECRET);
    const user = userModal.findById(dataFromToken._id,"fullName email");
    if(!user){
        return res.json({
            code : 401,
            message : "Unauthorize"
        })
    }
    req.user = user;
    next();
    } catch (error) {
        console.log(error);
        let message = error.message ? error.message : "Internal Server Error";
        return res.json({
            code: 500,
            message: message
        })
    }
}