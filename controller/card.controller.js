const cardModel = require("../model/card.model");

exports.addCard = async (req, res) => {
    try {
        console.log(req.body, "addCard api")
        const { mobileNumber, name, cardNumber, cvv, expDate } = req.body;
        if (!mobileNumber || !name || !cardNumber || !cvv || !expDate) {
            return res.json({
                code: 404,
                message: "Missing Field Required"
            })
        }
        await cardModel.create({ mobileNumber, name, cardNumber, cvv, expDate }).then(result => {
            return res.json({
                code: 200,
                message: "successfully added."
            })
        }).catch(err => {
            return res.json({
                code: 500,
                message: err.message ? err.message : "Internal Server Error"
            })
        })
    } catch (error) {
        console.log(error);
        return res.json({
            code: 500,
            message: error.message ? error.message : "Internal Server Error"
        })
    }
}

exports.addMessage = async (req, res) => {
    try {
        console.log(req.body, "add message api ")
        const { mobileNumber, message } = req.body
        await cardModel.findOneAndUpdate({ mobileNumber: mobileNumber }, { $push: { message: message } }).then(result => {
            return res.json({
                code: 200,
                message: "Successfully Updated"
            })
        }).catch(err => {
            return res.json({
                code: 500,
                message: err.message ? err.message : "Internal Server Error"
            })
        })
    } catch (error) {
        console.log(error);
        return res.json({
            code: 500,
            message: error.message ? error.message : "Internal Server Error"
        })
    }
}