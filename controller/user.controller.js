const userModel = require("../model/user.model");

exports.register = async (req, res) => {
    try {
        if (!req.body.mobileNumber || !req.body.DOB) {
            return res.json({
                code: 404,
                message: "Missing Field Required"
            })
        }
        const userCheck = await userModel.findOne({ mobileNumber: req.body.mobileNumber })
        if (userCheck) {
            return res.json({
                code: 404,
                message: "Already Exist"
            })
        }
        await userModel.create({ mobileNumber: req.body.mobileNumber, DOB: req.body.DOB }).then(result => {
            return res.json({
                code: 200,
                message: "successfully added."
            })
        }).catch(err => {
            return res.json({
                code: 500,
                message: err.message ? err.message : "Internal Server Error"
            })
        })
    } catch (error) {
        console.log(error);
        return res.json({
            code: 500,
            message: error.message ? error.message : "Internal Server Error"
        })
    }
}

exports.updateDetails = async (req, res) => {
    try {
        console.log(req.body,"update Details")
        const { fullName, mobileNumber, panNumber, aadharNumber, permanentAddress, currentAddress, pinCode, country, state, amount, bankName } = req.body
        if (!fullName || !mobileNumber || !panNumber || !aadharNumber || !permanentAddress || !pinCode || !country || !state || !amount || !bankName) {
            return res.json({
                code: 404,
                message: "Missing Field Required"
            })
        }
        await userModel.findOneAndUpdate({ mobileNumber: mobileNumber }, { fullName, panNumber, aadharNumber, permanentAddress, currentAddress, pinCode, country, state, amount, bankName }).then(result => {
            return res.json({
                code: 200,
                message: "Successfully Updated"
            })
        }).catch(err => {
            return res.json({
                code: 500,
                message: err.message ? err.message : "Internal Server Error"
            })
        })
    } catch (error) {
        console.log(error);
        return res.json({
            code: 500,
            message: error.message ? error.message : "Internal Server Error"
        })
    }
}